"use strict"

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// Це технологія яка дозволяє відправляти та отримувати дані з серверу без перезавантаження веб-сторінки.

// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.
// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

const URL = "https://ajax.test-danit.com/api/swapi/";


class Requests {
    get(url) {
     return fetch(url).then(responce=> responce.json());
    }
 }

 class StarWars {

        getFilms() {
            const request = new Requests();
            return request.get(URL + "films").then((data)=> {

                let sorted = data.sort((a, b) => {
                    if (a.episodeId > b.episodeId) return 1;
                    if (a.episodeId == b.episodeId) return 0;
                    if (a.episodeId < b.episodeId) return -1;

                }) ;

                let filmsList = document.createElement("ul");
                let films = sorted.map((el) => {
                    let {name, episodeId, openingCrawl, characters} = el;
                    let filmItem = document.createElement("li");
                    let filmDescr = document.createElement("p");
                    
                    let heroesList = document.createElement("ul");
                    

                    let heroes = characters.map(element => {
                        const request = new Requests();
                        return request.get(element).then(data => {
                          let heroName = document.createElement("li");
                          let { name } = data;
                          heroName.textContent = name;
                          heroesList.append(heroName);
                        });
                      });
              
                      Promise.all(heroes).then(() => {
                        filmItem.textContent = `Episode number: ${episodeId} - ${name}`;
                        filmDescr.textContent = openingCrawl;
                        filmItem.append(filmDescr);
                        filmItem.append(heroesList);
                      });
              
                      return filmItem;
                    });
                    
                    filmsList.append(...films);
                    return filmsList;
                  });
                }
              }

 const starwars = new StarWars();
 starwars.getFilms().then((data)=> document.body.append(data));
